import React from 'react';
import './App.css';
import { Container, Navbar, Nav, Form, FormControl, Button, Row, Col } from 'react-bootstrap';

function App() {
  return (
    <>
      <Navbar variant="dark" className="navbar">
        <Form inline>
          <FormControl type="text" placeholder="Search" />
          <Button bg="primary" >Search</Button>
        </Form>
      </Navbar>
      <div className="main-container">
        <Container className="sidenav">
          <Row className="brand" >
            <Col><label>Workflow FX</label></Col>
          </Row>
          <Row className="menu-item">
            <a><label>Menu Item</label></a>
          </Row>
          <Row className="menu-item">
            <a><label>Menu Item</label></a>
          </Row>
        </Container>
        <Container className="main-content">
          <div>Contenido 2</div>
        </Container>
      </div>
    </>
  );
}

export default App;
